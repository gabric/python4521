#!/usr/bin/env python3

import docker

docker_con = docker.DockerClient("192.168.0.200:2376")

container = docker_con.containers.run(
	'debian', '/bin/bash',
	name="learning", detach=True, tty=True
)

learning_container = docker_con.containers.get("learning")

print(learning_container.exec_run("ls -la"))
print(container.id)
print(container.name)