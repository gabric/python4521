from flask import Blueprint, render_template
import jenkins

jenkins_routes = Blueprint("jenkins", __name__, url_prefix="/jenkins")

@jenkins_routes.route("")
def index():
		return render_template("jenkins.html")