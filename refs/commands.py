#!/usr/bin/env python3

import docker

docker_con = docker.DockerClient("192.168.0.200:2376")

#for container in docker_con.containers.list(all=True):
#	print(container)

learn_container = docker_con.containers.get("learning")
learn_container.stop()
#print(learn_container.stats(stream=False))
learn_container.remove()