#!/usr/bin/env python3

import jenkins

jenkins_con = jenkins.Jenkins(
	"http://192.168.0.200:8080",
	username="admin",password="summer01"
)

###Printar informações básicas
#print(jenkins_con.get_whoami())
#print(jenkins_con.get_version())

### TO CREATE A JOB USING EMPTY_CONFIG TEMPLATE
#jenkins_con.create_job("Python-Job", jenkins.EMPTY_CONFIG_XML)


### TO BUILD A JOB AND PRINT IT'S RESULT
#queue = jenkins_con.build_job('Job1')
#print(jenkins_con.get_queue_item(queue))

### PRINT LISTA DE JOBS
#print(jenkins_con.get_jobs())

### LIST CONFIG FROM JOB
#print(jenkins_con.get_job_config("Python-Job"))

### CHANGE JOB CONFIG
#jenkins_con.reconfig_job("Python-Job", jenkins.EMPTY_CONFIG_XML)
